#!/bin/sh
# Copyright (C) 2016 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
set -e
tarball_url="https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.8.14.tar.xz"
tarball="$(basename ${tarball_url})"
size="1G"

create_empty_image()
{
	image="$1"
	size="$2"
	user="$(id -u)"
	group="$(id -g)"

	ddrescue /dev/zero "${image}" -s "${size}"
	mkfs.ext4 -E root_owner="${user}:${group}" "${image}"
}

download_tarball()
{
	url="$1"
	dest="$2"
	if [ ! -f "${dest}" ] ; then
		curl "${url}" > "${dest}"
	fi
}

mount_image()
{
	image="$1"
	target="$2"

	LANG="en_US.utf8" loop="$(udisksctl loop-setup -f ${image} --no-user-interaction | sed 's#^Mapped file .* as ##' | sed 's#\.$##')"
	LANG="en_US.utf8" mount_path="$(udisksctl mount -b ${loop} | sed 's#^Mounted /dev/loop.* at ##' | sed 's#\.$##')"
	rm -f "${target}"
	ln -s "${mount_path}" "${target}"
}

umount_image()
{
	target="$1"

	mount_path="$(readlink ${target})"
	loop_block="$(cat /proc/mounts | grep " ${mount_path} " | awk '{print $1}')"
	udisksctl unmount -b "${loop_block}"
	udisksctl loop-delete -b "${loop_block}"
	rm -f "${target}"
}

extract_tarball()
{
	tarball="$1"
	target="$2"

	# We need more than just lost+found
	if [ "$(ls -ld ${target}/* | wc -l)" -le 1 ] ; then
		tar xf "${tarball}" -C "${target}"
	fi
}

create_image()
{
	image="$1"
	mount_dir="$(mktemp -d -u)"
	download_tarball "${tarball_url}" "${tarball}"
	create_empty_image "${image}" "${size}"
	mount_image "${image}" "${mount_dir}"
	extract_tarball "${tarball}" "${mount_dir}"
	cp -f "${tarball}" "${mount_dir}/${tarball}"
	sudo umount "${mount_dir}"
	echo "DONE"
}

usage()
{
	echo "Available commands:"
	echo "  create <image>"
	echo "  mount <image> <target-path>"
	echo "  umount <target-path>"
}

if [ $# -eq 2 -a "$1" = "create" ] ; then
	set -x
	"$1_image" "$2"
elif [ $# -eq 3 -a "$1" = "mount" ] ; then
	"$1_image" "$2" "$3"
elif [ $# -eq 2 -a "$1" = "umount" ] ; then
	"$1_image" "$2"
else
	usage
fi
